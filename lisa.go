package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

type Issue struct {
	Id         int
	Iid        int
	Project_id int
	Title      string
	State      string
	Web_url	   string
	Labels     []string
}

type User struct {
	Username string
	Id       int
}

type MergeRequest struct {
	Id         int
	Iid        int
	Project_id int
	Title      string
	Web_url	   string
	Labels     []string
	Author     User
	Assignee   User
}

const labelReady = "workflow::ready for development"
const labelInDev = "workflow::In dev"
const labelInReview = "workflow::In review"

func gitLabGet(path string, target interface{}) error {
	s := []string{"https://gitlab.com/api/v4/", path}
	url := strings.Join(s, "")

	client := &http.Client{}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	req.Header.Set("PRIVATE-TOKEN", os.Getenv("GITLAB_TOKEN"))
	req.Header.Set("User-Agent", "lisa-go-client")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	return json.NewDecoder(resp.Body).Decode(target)
}

func gitLabPut(path string, values url.Values) error {
	s := []string{"https://gitlab.com/api/v4/", path}
	url := strings.Join(s, "")

	client := &http.Client{}
	req, err := http.NewRequest(http.MethodPut, url, nil)
	req.URL.RawQuery = values.Encode()
	req.Header.Set("PRIVATE-TOKEN", os.Getenv("GITLAB_TOKEN"))
	req.Header.Set("User-Agent", "lisa-go-client")

	resp, err := client.Do(req)

	if err != nil {
		log.Fatal(err)
	}

	_, err = ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	// TODO: refresh the status of the Issue in memory

	return err
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func mergeRequestToRef(mergeRequest MergeRequest) string {
	url, err := url.Parse(mergeRequest.Web_url)
	if err != nil {
		log.Fatal(err)
	}
	return strings.Replace(url.Path, "/merge_requests/", "!", -1)[1:]
}

func issueToRef(issue Issue) string {
	url, err := url.Parse(issue.Web_url)
	if err != nil {
		log.Fatal(err)
	}
	return strings.Replace(url.Path, "/issues/", "#", -1)[1:]
}

func labelsWithColumn(labels []string, column string) string {
	newLabels := []string{}
	for _, b := range labels {
		if (b != labelReady) && (b != labelInDev) && (b != labelInReview) {
			newLabels = append(newLabels, b)
		}
	}
	if len(column) > 0 {
		newLabels = append(newLabels, column)
	}
	return strings.Join(newLabels, ",")
}

func openIssuesAssignedToMe(issues *[]Issue) {
	err := gitLabGet("issues?state=opened&scope=assigned-to-me", issues)
	if err != nil {
		log.Fatal(err)
	}
}

func mergeRequestsClosingIssue(issue Issue, mergeRequests *[]MergeRequest) {
	path := fmt.Sprintf("projects/%d/issues/%d/closed_by", issue.Project_id, issue.Iid)
	err := gitLabGet(path, mergeRequests)
	if err != nil {
		log.Fatal(err)
	}
}

func logChanges(issue Issue, mergeRequest MergeRequest, label string) {
	msg := "<article>\n"
	msg += fmt.Sprintf("<h2><a href=\"%s\">%s</a> %s</h2>\n", issue.Web_url, issueToRef(issue), issue.Title)
	msg += "<p>moved by "
	msg += fmt.Sprintf("<a href=\"%s\">%s</a>", mergeRequest.Web_url, mergeRequestToRef(mergeRequest))
	msg += " to "
	msg += "<span>" + label + "</span>"
	msg += " at <em>" + time.Now().Format("2006-01-02 15:04:05") + "</em>\n"
	msg += "</p>\n"

	f, err := os.OpenFile("CHANGES.html", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	if _, err = f.WriteString(msg); err != nil {
		log.Fatal(err)
	}
}

func moveToInDev(issue Issue, mergeRequest MergeRequest) {
	if stringInSlice(labelInDev, issue.Labels) || stringInSlice(labelInReview, issue.Labels) {
		log.Printf("  └─ already ~\"%s\"", labelInDev)
		return
	}
	path := fmt.Sprintf("projects/%d/issues/%d", issue.Project_id, issue.Iid)
	labels := labelsWithColumn(issue.Labels, labelInDev)
	data := url.Values{}
	data.Set("labels", labels)
	gitLabPut(path, data)
	log.Printf("  └─ moved to ~\"%s\"", labelInDev)
	logChanges(issue, mergeRequest, labelInReview)
}

func moveToInReview(issue Issue, mergeRequest MergeRequest) {
	if stringInSlice(labelInReview, issue.Labels) {
		log.Printf("  └─ already ~\"%s\"", labelInReview)
		return
	}
	path := fmt.Sprintf("projects/%d/issues/%d", issue.Project_id, issue.Iid)
	fmt.Println(path)
	labels := labelsWithColumn(issue.Labels, labelInReview)
	fmt.Println(labels)
	data := url.Values{}
	data.Set("labels", labels)
	gitLabPut(path, data)
	log.Printf("  └─ moved to ~\"%s\"", labelInReview)
	logChanges(issue, mergeRequest, labelInReview)
}

func moveOpenIssues() {
	issues := new([]Issue)
	openIssuesAssignedToMe(issues)

	for _, issue := range *issues {
		log.Printf("%-30s: %s\n", issueToRef(issue), issue.Title)
		mergeRequests := new([]MergeRequest)
		mergeRequestsClosingIssue(issue, mergeRequests)
		for _, mr := range *mergeRequests {
			log.Printf("  ├─ %-30s: %s\n", mergeRequestToRef(mr), mr.Title)

			if mr.Author.Id != mr.Assignee.Id {
				// Label issue ~"In review" if the closing MR is assigned to someone else
				moveToInReview(issue, mr)
			} else {
				// Label issue ~"In dev" if an MR closing it exists
				moveToInDev(issue, mr)
			}
		}
	}
}

func main() {
	moveOpenIssues()
}
